// The main header for the graphics framework
#include <graphics_framework.h>
#include <glm\glm.hpp>

// The namespaces we are using
using namespace std;
using namespace graphics_framework;
using namespace glm;

// Create a mesh called terrainplane which will be the terrainplane once the heightmap is added in
mesh terrainplane;
// Create a mesh called skybox which holds the images of the sky
mesh skybox;
// A library of meshes
map<string, mesh> meshes;

// Creation of Multiple effects
// Effect terrainshader which is uses a pixel/frag and vertex shader
effect terrainshader; // Previously eff
effect blendshader; // Previously eff2
effect watershader; // Previously eff3
effect simplediffuseshader; // Previously eff4
effect combinedlightingshader; // Previously eff5
effect skyboxeffect;
effect skyboximages;

// Cubemap which is used to create the sky effect
cubemap cube_map;

// Array of textures which are assigned images later
texture tex[5];
// The blend map texture
texture blend_map;

// Delcaring the different types of cameras
target_camera targetcam;
free_camera freecam;
// Used for the free camera
double cursor_x = 0.0;
double cursor_y = 0.0;
// CameraID which is used to change between the different cameras
int cameraID = 0;

// Temporary light 
directional_light light;

float s = 0.0f;
float total_time = 0.0f;

// Method called initialise which is used to hide the cursor
bool initialise()
{
	// Set input mode and hide the cursor
	glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwGetCursorPos(renderer::get_window(), &cursor_x, &cursor_y);

	return true;
}

// Method called generate_terrain which takes in a black in white png of an image of a box with a circlular radial gradient in the middle to create an island.
// To generate the terrain we use this image as a heightmap. The colours of the pixels are converted to height data on a plane giving us the y value. If the 
// pixel is lighter (white) then the y-component will be turn out to have a larger y-value and therefore be at the top of the plane whereas if the pixel is darker
// (black) then the y-component will turn out to have a lower y-value and therefore will be found at the bottom of the plane.
mesh generate_terrain(const texture &height_map, unsigned int width, unsigned int depth, float height_scale)
{
	geometry geom;
	// Contains our position data
	vector<vec3> positions;
	// Contains our normal data
	vector<vec3> normals;
	// Contains our texture coordinate data
	vector<vec2> tex_coords;
	// Contains our texture weights
	vector<vec4> tex_weights;
	// Contains our index data
	vector<unsigned int> indices;

	// Extract the texture data from the image
	glBindTexture(GL_TEXTURE_2D, height_map.get_id());
	auto data = new vec4[height_map.get_width() * height_map.get_height()];
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, (void*)data);

	// Determine ratio of height map to geometry
	float width_point = static_cast<float>(width) / static_cast<float>(height_map.get_width());
	float depth_point = static_cast<float>(depth) / static_cast<float>(height_map.get_height());

	// Point to work on
	vec3 point;

	// Iterate through each point, calculate vertex and add to vector
	for (int x = 0; x < height_map.get_width(); ++x)
	{
		// Calculate x position of point
		point.x = -(width / 2.0f) + (width_point * static_cast<float>(x));

		for (int z = 0; z < height_map.get_height(); ++z)
		{
			// Calculate z position of point
			point.z = -(depth / 2.0f) + (depth_point * static_cast<float>(z));
			// Y position based on red component of height map data
			point.y = data[(z * height_map.get_width()) + x].y * height_scale;
			// Add point to position data
			positions.push_back(point);
		}
	}

	// Part 1 - Add index data
	for (unsigned int x = 0; x < height_map.get_width() - 1; ++x)
	{
		for (unsigned int y = 0; y < height_map.get_height() - 1; ++y)
		{
			// Get four corners of patch
			unsigned int top_left = (y * height_map.get_width()) + x;
			unsigned int top_right = (y * height_map.get_width()) + x + 1;
			unsigned int bottom_left = ((y + 1) * height_map.get_width()) + x;
			unsigned int bottom_right = ((y + 1) * height_map.get_width()) + x + 1;
			// Push back indices for triangle 1
			indices.push_back(top_left);
			indices.push_back(bottom_right);
			indices.push_back(bottom_left);
			// Push back indices for triangle 2
			indices.push_back(top_left);
			indices.push_back(top_right);
			indices.push_back(bottom_right);
		}
	}

	// Resize the normals buffer
	normals.resize(positions.size());

	// Part 2 - Calculate normals for the height map
	for (unsigned int i = 0; i < indices.size() / 3; ++i)
	{
		// Get indices for the triangle
		auto idx1 = indices[i * 3];
		auto idx2 = indices[i * 3 + 1];
		auto idx3 = indices[i * 3 + 2];

		// Calculate two sides of the triangle
		vec3 side1 = positions[idx1] - positions[idx3];
		vec3 side2 = positions[idx1] - positions[idx2];

		// Normal is cross product of these two sides
		vec3 normal = normalize(cross(side2, side1));

		// Add to normals in the normal buffer using the indices for the triangle
		normals[idx1] += normal;
		normals[idx2] += normal;
		normals[idx3] += normal;
	}

	// Part 2 - Normalize all the normals
	for (auto &n : normals)
	{
		n = normalize(n);
	}
	// Part 3 - Add texture coordinates for geometry
	for (unsigned int x = 0; x < height_map.get_width(); ++x)
	{
		for (unsigned int z = 0; z < height_map.get_height(); ++z)
		{
			tex_coords.push_back(vec2(width_point * x, depth_point * z));
		}
	}

	// Part 4 - Calculate texture weights for each vertex
	for (unsigned int x = 0; x < height_map.get_width(); ++x)
	{
		for (unsigned int z = 0; z < height_map.get_height(); ++z)
		{
			// Calculate tex weight
			vec4 tex_weight(
				clamp(1.0f - abs(data[(height_map.get_width() * z) + x].y - 0.0f) / 0.25f, 0.0f, 1.0f),
				clamp(1.0f - abs(data[(height_map.get_width() * z) + x].y - 0.15f) / 0.25f, 0.0f, 1.0f),
				clamp(1.0f - abs(data[(height_map.get_width() * z) + x].y - 0.5f) / 0.25f, 0.0f, 1.0f),
				clamp(1.0f - abs(data[(height_map.get_width() * z) + x].y - 0.9f) / 0.25f, 0.0f, 1.0f));

			// Sum the components of the vector
			auto total = tex_weight.x + tex_weight.y + tex_weight.z + tex_weight.w;

			// Divide weight by sum
			tex_weight /= total;

			// Add tex weight to weights
			tex_weights.push_back(tex_weight);
		}
	}

	// Add necessary buffers to the geometry
	geom.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER);
	geom.add_buffer(normals, BUFFER_INDEXES::NORMAL_BUFFER);
	geom.add_buffer(tex_coords, BUFFER_INDEXES::TEXTURE_COORDS_0);
	geom.add_buffer(tex_weights, BUFFER_INDEXES::TEXTURE_COORDS_1);
	geom.add_index_buffer(indices);

	// Delete data
	delete[] data;

	return mesh(geom);
}

// Method which creates meshes, adds shaders, builds effects, loads in textures and sets basic camera properties
bool load_content()
{
	// Plane create for water
	meshes["waterplane"] = mesh(geometry_builder::create_plane());
	// Transform the position of water plane and increase the y-component by 1.0f so it is not under the terrainplane
	meshes["waterplane"].get_transform().position = uvec3(0.0f, 1.0f, 0.0f);
	// Transform the scale of the plane to fit the terrainplane
	meshes["waterplane"].get_transform().scale = vec3(0.2f, 0.2f, 0.2f);
	
	// Load is sphere which is used to show lighting effect
	meshes["sphere"] = mesh(geometry_builder::create_sphere(20, 20));
	meshes["sphere"].get_transform().position = vec3(0.0f, 5.0f, 0.5f);

	// Create a box which rotates 
	meshes["rotatingbox"] = mesh(geometry_builder::create_box());
	meshes["rotatingbox"].get_transform().position = vec3(0.0f, 5.0f, 10.0f);

	meshes["scalingsphere"] = mesh(geometry_builder::create_sphere());
	meshes["scalingsphere"].get_transform().position = vec3(0.0f, 10.0f, 0.0f);

	meshes["transformationbox"] = mesh(geometry_builder::create_box());
	meshes["transformationbox"].get_transform().position = vec3(0.0f, 10.0f, 3.0f);

	// Load height map
	texture height_map("..\\resources\\textures\\heightmaps\\terrain4.png");
	// Use geometry to create terrain mesh
	meshes["terrainplane"] = generate_terrain(height_map, 20, 20, 2.0f);

	// Create box geometry for skybox
	geometry skyboxgeom;
	// Use Quads to create the cube
	skyboxgeom.set_type(GL_QUADS);
	// Add all the positions of the cube
	vector<vec3> positions
	{
		vec3(-1.0f, 1.0f, -1.0f),	/* 5 */
		vec3(-1.0f, 1.0f, 1.0f),	/* 1 */
		vec3(-1.0f, -1.0f, 1.0f),	/* 2 */
		vec3(-1.0f, -1.0f, -1.0f),	/* 7 */
		vec3(1.0f, 1.0f, 1.0f),		/* 4 */
		vec3(1.0f, 1.0f, -1.0f),	/* 6 */
		vec3(1.0f, -1.0f, -1.0f),	/* 8 */
		vec3(1.0f, -1.0f, 1.0f),	/* 3 */
		vec3(-1.0f, 1.0f, -1.0f),	/* 5 */
		vec3(1.0f, 1.0f, -1.0f),	/* 6 */
		vec3(1.0f, 1.0f, 1.0f),		/* 4 */
		vec3(-1.0f, 1.0f, 1.0f),	/* 1 */
		vec3(-1.0f, -1.0f, 1.0f),	/* 2 */
		vec3(1.0f, -1.0f, 1.0f),	/* 3 */
		vec3(1.0f, -1.0f, -1.0f),	/* 8 */
		vec3(-1.0f, -1.0f, -1.0f),	/* 7 */
		vec3(-1.0f, -1.0f, -1.0f),	/* 7 */
		vec3(1.0f, -1.0f, -1.0f),	/* 8 */
		vec3(1.0f, 1.0f, -1.0f),	/* 6 */
		vec3(-1.0f, 1.0f, -1.0f),	/* 5 */
		vec3(1.0f, -1.0f, 1.0f),	/* 3 */
		vec3(-1.0f, -1.0f, 1.0f),	/* 2 */
		vec3(-1.0f, 1.0f, 1.0f),	/* 1 */
		vec3(1.0f, 1.0f, 1.0f)		/* 4 */
	};
	skyboxgeom.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER);

	skybox = mesh(skyboxgeom);
	skybox.get_transform().scale = vec3(100, 100, 100);
	meshes["skybox"] = skybox;
	// Scale the skybox up by 100 to add distance
	
	// Load the cubemap with an array of six filenames equaling +x, -x, +y, -y, +z, -z
	array<string, 6> filenames =
	{
		"..\\resources\\textures\\cubemaps\\alien\\posx.png",
		"..\\resources\\textures\\cubemaps\\alien\\negx.png",
		"..\\resources\\textures\\cubemaps\\alien\\posy.png",
		"..\\resources\\textures\\cubemaps\\alien\\negy.png",
		"..\\resources\\textures\\cubemaps\\alien\\posz.png",
		"..\\resources\\textures\\cubemaps\\alien\\negz.png"
	};
	// Create cube_map with the array of images
	cube_map = cubemap(filenames);

	// Load in shaders
	// Terrain vertex shader
	terrainshader.add_shader("terrain.vert", GL_VERTEX_SHADER);
	// Terrain pixel/fragment shader
	terrainshader.add_shader("terrain.frag", GL_FRAGMENT_SHADER);
	// Terrain direction shader
	terrainshader.add_shader("..\\resources\\shaders\\parts\\direction.frag", GL_FRAGMENT_SHADER);
	terrainshader.add_shader("..\\resources\\shaders\\parts\\weighted_texture.frag", GL_FRAGMENT_SHADER);
	
	// Blend Shaders
	blendshader.add_shader("..\\resources\\shaders\\blend.vert", GL_VERTEX_SHADER);
	blendshader.add_shader("..\\resources\\shaders\\blend.frag", GL_FRAGMENT_SHADER);

	// Pixel/fragment and vertex Shaders for the water. Transparent code contained within shader
	watershader.add_shader("..\\resources\\shaders\\watershader.vert", GL_VERTEX_SHADER);
	watershader.add_shader("..\\resources\\shaders\\watershader.frag", GL_FRAGMENT_SHADER);
	
	// Pixel/fragment and vertex Shaders for simple diffuse lighting
	simplediffuseshader.add_shader("..\\resources\\shaders\\simple_diffuse.vert", GL_VERTEX_SHADER);
	simplediffuseshader.add_shader("..\\resources\\shaders\\simple_diffuse.frag", GL_FRAGMENT_SHADER);

	// Pixel/fragment and vertex Shaders for combined lighting
	combinedlightingshader.add_shader("..\\resources\\shaders\\combined_lighting.vert", GL_VERTEX_SHADER);
	combinedlightingshader.add_shader("..\\resources\\shaders\\combined_lighting.frag", GL_FRAGMENT_SHADER);

	skyboximages.add_shader("skyboximagesshader.vert", GL_VERTEX_SHADER);
	skyboximages.add_shader("skyboximagesshader.frag", GL_FRAGMENT_SHADER);

	// Set the position of the camera to the centre of the skybox
	skybox.get_transform().position = targetcam.get_position();
	skyboxeffect.add_shader("..\\resources\\shaders\\skyboxshader.vert", GL_VERTEX_SHADER);
	skyboxeffect.add_shader("..\\resources\\shaders\\skyboxshader.frag", GL_FRAGMENT_SHADER);

	// Build all the effects/shaders
	terrainshader.build();
	blendshader.build();
	watershader.build();
	simplediffuseshader.build();
	combinedlightingshader.build();
	skyboximages.build();
	skyboxeffect.build();

	// Temporary Lighting
	light.set_ambient_intensity(vec4(0.3f, 0.3f, 0.3f, 1.0f));
	light.set_light_colour(vec4(0.9f, 0.9f, 0.9f, 1.0f));
	light.set_direction(normalize(vec3(1.0f, 1.0f, 1.0f)));
	terrainplane.get_material().set_diffuse(vec4(0.5f, 0.5f, 0.5f, 1.0f));
	terrainplane.get_material().set_specular(vec4(0.0f, 0.0f, 0.0f, 1.0f));
	terrainplane.get_material().set_shininess(20.0f);
	terrainplane.get_material().set_emissive(vec4(0.0f, 0.0f, 0.0f, 1.0f));

	// Different textures which are loaded in from the texture folder
	tex[0] = texture("..\\resources\\textures\\rock.dds");
	tex[1] = texture("..\\resources\\textures\\sand.dds");
	tex[2] = texture("..\\resources\\textures\\sand-texture1.jpg");
	tex[3] = texture("..\\resources\\textures\\grass.png");
	tex[4] = texture("..\\resources\\textures\\water.png");

	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());

	// Set camera properties
	targetcam.set_position(vec3(10.0f, 10.0f, 10.0f));
	targetcam.set_target(vec3(0.0f, 0.0f, 0.0f));
	targetcam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);

	freecam.set_position(vec3(0.0f, 5.0f, 0.0f));
	freecam.set_target(vec3(0.0f, 0.0f, 0.0f));
	freecam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	return true;
}

bool update(float delta_time)
{
	// Target Camera Positons
	vec3 v1 = vec3(10, 10, 10);
	vec3 v2 = vec3(-10, 10, 10);
	vec3 v3 = vec3(-10, 10, -10);
	vec3 v4 = vec3(10, 10, -10);

	// Buttons pressed to move target camera
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_1))
	{
		// Move the target camera to position vector1
		targetcam.set_position(v1);
		// Set camera ID to 0 to use the target camera
		cameraID = 0;
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_2))
	{
		// Move the target camera to position vector1
		targetcam.set_position(v2);
		// Set camera ID to 0 to use the target camera
		cameraID = 0;
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_3))
	{
		// Move the target camera to position vector1
		targetcam.set_position(v3);
		// Set camera ID to 0 to use the target camera
		cameraID = 0;
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_4))
	{
		// Move the target camera to position vector1
		targetcam.set_position(v4);
		// Set camera ID to 0 to use the target camera
		cameraID = 0;
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_F))
	{
		glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		// Set camera ID to 0 to use the free camera
		cameraID = 1;
	}
	
	// The ratio of pixels to rotation - remember the fov
	static double ratio_width = quarter_pi<float>() / static_cast<float>(renderer::get_screen_width());
	static double ratio_height = (quarter_pi<float>() * (static_cast<float>(renderer::get_screen_height()) / static_cast<float>(renderer::get_screen_width()))) / static_cast<float>(renderer::get_screen_height());

	// Create current x and y to store the current x and y mouse values
	double current_x;
	double current_y;
	// Get the current cursor position
	glfwGetCursorPos(renderer::get_window(), &current_x, &current_y);
	// Calculate delta of cursor positions from last frame
	float delta_x = current_x - cursor_x;
	float delta_y = current_y - cursor_y;

	// Multiply deltas by ratios defined above and gets the change in orientation
	delta_x = delta_x * ratio_width;
	delta_y = delta_y * ratio_height * -1;

	// Rotate the free camera by delta x and y
	freecam.rotate(delta_x, delta_y);

	// Use keyboard buttons WSAD to move the free camera
	// Change the value to adjust the speed
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_W))
	{
		// Make free camera move when W is pressed
		freecam.move(vec3(0.0f, 0.0f, 10.0f) * delta_time);
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_A))
	{
		// Make free camera move when A is pressed
		freecam.move(vec3(-10.0f, 0.0f, 0.0f) * delta_time);
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_S))
	{
		// Make free camera move when S is pressed
		freecam.move(vec3(0.0f, 0.0f, -10.0f) * delta_time);
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_D))
	{
		// Make free camera move when D is pressed
		freecam.move(vec3(10.0f, 0.0f, 0.0f) * delta_time);
	}

	// Rotate the sphere and rotatingbox
	meshes["sphere"].get_transform().rotate(vec3(0.0f, half_pi<float>(), 0.0f) * delta_time);
	meshes["rotatingbox"].get_transform().rotate(vec3(0.0f, half_pi<float>(), half_pi<float>()) * delta_time);

	total_time += delta_time;
	// Update the scale - base on sin wave
	s = 1.0f + sinf(total_time);
	// Multiply by 5
	s *= 0.5f;
	meshes["scalingsphere"].get_transform().scale = vec3(s, s, s);
	// Update the cameras
	targetcam.update(delta_time);
	freecam.update(delta_time);

	// Update the cursors 
	cursor_x = current_x;
	cursor_y = current_y;

	// Set the position of the camera to the centre of the skybox
	meshes["skybox"].get_transform().position = targetcam.get_position();

	return true;
}

// Our rendering code will go in here
bool render()
{
	// Getting the view and projection by default
	auto V = targetcam.get_view();
	auto P = targetcam.get_projection();

	// If camera id is 0 then (for target cameras)
	if (cameraID == 0)
	{
		// Get the view and projection
		V = targetcam.get_view();
		P = targetcam.get_projection();
	}
	//If camera id is 1 then (for free camera)
	if (cameraID == 1)
	{
		// Get the view and projection
		V = freecam.get_view();
		P = freecam.get_projection();
	}

	// Render meshes
	for (auto &e : meshes)
	{
		mesh m = e.second;
		if (e.first == "skybox")
		{
			// Disable depth test and depth mask
			glDisable(GL_DEPTH_TEST);
			glDepthMask(GL_FALSE);
			// Bind skybox effect
			renderer::bind(skyboxeffect);
			auto M = e.second.get_transform().get_transform_matrix();
			auto MVP = P * V *M; // Change!!!
			// Set MVP matrix uniform
			glUniformMatrix4fv(
				skyboxeffect.get_uniform_location("MVP"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(MVP)); // Pointer to matrix data
			// Set cubemap uniform
			renderer::bind(cube_map, 0);
			// Render skybox
			renderer::render(skybox);
			// Enable depth test and depth mask
			glEnable(GL_DEPTH_TEST);
			glDepthMask(GL_TRUE);
		}
		if (e.first == "waterplane")
		{
			glAlphaFunc(GL_GREATER, 0.2f);
			glEnable(GL_ALPHA_TEST);
			// Bind effect
			renderer::bind(watershader);
			// Create MVP matrix
			auto M = m.get_transform().get_transform_matrix();
			auto MVP = P * V * M;
			// Set MVP matrix uniform
			glUniformMatrix4fv(
				watershader.get_uniform_location("MVP"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(MVP)); // Pointer to matrix data

			// Bind and set texture
			renderer::bind(tex[4], 0);

			// Set the uniform value for textures
			glUniform1i(watershader.get_uniform_location("tex[4]"), 0);
			glDisable(GL_ALPHA_TEST);
		}
		if (e.first == "terrainplane")
		{
			// Bind effect
			renderer::bind(terrainshader);
			// Create MVP matrix
			auto M = terrainplane.get_transform().get_transform_matrix();
			auto MVP = P * V * M;
			// Set MVP matrix uniform
			glUniformMatrix4fv(
				terrainshader.get_uniform_location("MVP"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(MVP)); // Pointer to matrix data

			// Setting Unifrom
			glUniform4fv(terrainshader.get_uniform_location("colour"), 1, value_ptr(vec4(0.7f, 0.7f, 0.7f, 1.0f)));

			// ?????????? change these to colour
			glUniformMatrix4fv(
				terrainshader.get_uniform_location("M"),
				1,
				GL_FALSE,
				value_ptr(M));
			glUniformMatrix3fv(
				terrainshader.get_uniform_location("N"),
				1,
				GL_FALSE,
				value_ptr(terrainplane.get_transform().get_normal_matrix()));
			renderer::bind(terrainplane.get_material(), "mat");
			renderer::bind(light, "light");
			renderer::bind(tex[0], 0);
			glUniform1i(terrainshader.get_uniform_location("tex[0]"), 0);
			renderer::bind(tex[1], 1);
			glUniform1i(terrainshader.get_uniform_location("tex[1]"), 1);
			renderer::bind(tex[2], 2);
			glUniform1i(terrainshader.get_uniform_location("tex[2]"), 2);
			renderer::bind(tex[3], 3);
			glUniform1i(terrainshader.get_uniform_location("tex[3]"), 3);
			glUniform3fv(terrainshader.get_uniform_location("eye_pos"), 1, value_ptr(targetcam.get_position()));
		}
		if (e.first == "sphere")
		{
			// Bind effect
			renderer::bind(simplediffuseshader);
			auto M = m.get_transform().get_transform_matrix();
			auto MVP = P * V * M;
			// Set MVP matrix uniform
			glUniformMatrix4fv(
				simplediffuseshader.get_uniform_location("MVP"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(MVP)); // Pointer to matrix data

			mat3 whatever = m.get_transform().get_normal_matrix();
			glUniformMatrix3fv(simplediffuseshader.get_uniform_location("N"), 1, GL_FALSE, value_ptr(whatever));
			glUniform4fv(simplediffuseshader.get_uniform_location("material_colour"), 1, value_ptr(vec4(1.0f, 0.0f, 0.0f, 1.0f)));
			glUniform4fv(simplediffuseshader.get_uniform_location("light_colour"), 1, value_ptr(vec4(1.0f, 1.0f, 1.0f, 1.0f)));
			glUniform3fv(simplediffuseshader.get_uniform_location("light_dir"), 1, value_ptr(vec3(1.0f, 1.0f, -1.0f)));

		}
		if (e.first == "rotatingbox")
		{

			// Bind effect
			renderer::bind(simplediffuseshader);
			// Create MVP matrix
			auto M = m.get_transform().get_transform_matrix();
			auto MVP = P * V * M;
			// Set MVP matrix uniform
			glUniformMatrix4fv(
				simplediffuseshader.get_uniform_location("MVP"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(MVP)); // Pointer to matrix data

			// Bind and set texture
			renderer::bind(tex[4], 0);

			// Set the uniform value for textures
			glUniform1i(simplediffuseshader.get_uniform_location("tex[3]"), 0);
		}
		if (e.first == "scalingsphere")
		{
			// Bind effect
			renderer::bind(simplediffuseshader);
			// Create MVP matrix
			mat4 M = m.get_transform().get_transform_matrix();
			auto MVP = P * V * M;
			// Set MVP matrix uniform
			glUniformMatrix4fv(
				simplediffuseshader.get_uniform_location("MVP"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(MVP)); // Pointer to matrix data

			// Bind and set texture
			renderer::bind(tex[0], 0);

			// Set the uniform value for textures
			glUniform1i(simplediffuseshader.get_uniform_location("tex[0]"), 0);
		}
		if (e.first == "transformationbox")
		{
			// Bind effect
			renderer::bind(simplediffuseshader);
			// Create MVP matrix
			mat4 T = translate(mat4(1.0f), vec3(4.0f, 4.0f, 0.0f));
			mat4 S = scale(mat4(2.0f), vec3(s));
			mat4 R = rotate(mat4(1.0f), pi<float>(), vec3(0.0f, 0.0f, 1.0f));
			mat4 M = T * (R * S);
			//mat4 M = m.get_transform().get_transform_matrix();
			auto MVP = P * V * M;
			// Set MVP matrix uniform
			glUniformMatrix4fv(
				simplediffuseshader.get_uniform_location("MVP"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(MVP)); // Pointer to matrix data

			// Bind and set texture
			renderer::bind(tex[0], 0);

			// Set the uniform value for textures
			glUniform1i(simplediffuseshader.get_uniform_location("tex[0]"), 0);
		}

		// Render mesh
		if (e.first != "waterplane")renderer::render(m);
	}

	mesh m = meshes["waterplane"];

	glAlphaFunc(GL_GREATER, 0.2f);
	glEnable(GL_ALPHA_TEST);
	// Bind effect
	renderer::bind(watershader);
	// Create MVP matrix
	auto M = m.get_transform().get_transform_matrix();
	auto MVP = P * V * M;
	// Set MVP matrix uniform
	glUniformMatrix4fv(
		watershader.get_uniform_location("MVP"), // Location of uniform
		1, // Number of values - 1 mat4
		GL_FALSE, // Transpose the matrix?
		value_ptr(MVP)); // Pointer to matrix data

	// Bind and set texture
	renderer::bind(tex[4], 0);

	// Set the uniform value for textures
	glUniform1i(watershader.get_uniform_location("tex[4]"), 0);
	glDisable(GL_ALPHA_TEST);
	renderer::render(m);
	
	return true;
}

// Main Method
void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);
	// Run application
	application.run();
}